# Academy AWS Ebased Exercise

Serverless Application to sign-up users, assign a credit card (gold or classic) and a gift.

# Stack

Lambdas:

- createClient: Create clients with ID(DNI), firstName, lastName and birthDate. It should only allow adults (+18) up to 65 years of age.
- updateClient: If the birthDate is updated, the card and the gift are regenerated
- deleteClient: Logical Delete
- getClientById: Search by DNI
- getAllClients: List of all Clients (active/inactive)
- clientCard: Grant a credit card (Classic if you are under 45 years old and Gold if you are older) generating a random number, expiration date and security code.
- clientGift: Assign a gift according to the season that corresponds to the customer's birthday date; summer: t-shirt, autumn: sweater, winter: sweatshirt, spring: shirt
- createPurchase: Allows to charge purchases indicating the customer's DNI. They are made up of an array of objects that include the name of the product and the original price. It will only allow you to make purchases with active customers.


The final price attribute will be automatically added to each product, which will be the result of the original price applying a discount according to the card that the customer has, if it is Gold it will be 12% while if it is Classic it will be 8%

Each purchase (which in turn can include several products) will have its own ID and the customer's ID will count as an attribute.

Every $200 of purchase (in final price) the customer will be awarded 1 point. They will accumulate in the customer's record.

SQS:

- cardQueue
- giftQueue

SNS

- clientsTopic
- GiftSubscription
- CardSubscription

DynamoDB

- ClientsDynamoDbTable
- PurchaseDynamoDbTable

API Gateway

- /clients POST
- /clients GET
    - /{id} DELETE
    - /{id} GET
    - /{id} PUT
- /purchase POST

IAM Policies

- snsToGiftQueueSqsPolicy
- snsTocardQueueSqsPolicy


## Dependencies

Need to install the dependency serverless-iam-roles-per-function, ebased, uuid

```javascript
npm install
```

To be able to deploy the application it is necessary to install serverless

```javascript
npm i serverless
```

- AWS Credentials


## Deploy

Configure your AWS credentials (.aws/credentials)

To be able to deploy the application it is necessary to install serverless

```javascript
npm i serverless
```

```javascript
serverless deploy --verbose
```

## Body Create Client

Required fields:

```javascript
{
    "dni":"12345678",
    "firstName": "Michael",
    "lastName": "Scott",
    "birthDate": "1990-03-27"
}
```

## Body Create Purchase

Required fields:

```javascript
{
    "dni": "12345678",
    "products": [
        {
            "name": "Samsung A32",
            "price": 30000
        }
    ]
}
```

## Remove Stack


```javascript
serverless remove
```