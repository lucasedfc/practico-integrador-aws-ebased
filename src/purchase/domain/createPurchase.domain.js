//@ts-check

const { ErrorHandled } = require('ebased/util/error');
const { v4: uuidv4 } = require('uuid');
const { getClientByIdService } = require('../../client/service/getClientById.service');
const { updateClientService } = require('../../client/service/updateClient.service');
const { descByClient } = require('../helper/calcDiscount.helper');
const { CreatePurchaseValidation } = require('../schema/input/createPurchase.input');
const { createPurchaseService } = require('../service/createPurchase.service');


const createPurchaseDomain = async (commandPayload, commandMeta) => {
    
        commandPayload.id = uuidv4();
        const data = new CreatePurchaseValidation(commandPayload, commandMeta).get();
        const client = await getClientByIdService(data.dni);

        console.log({client});
        if(client.disabled) {
            throw new ErrorHandled('Client Disabled', { status: 401, code: 'UNAUTHORIZED', layer: 'CLIENT'});
        }

        const { purchaseData, clientData } = descByClient(data, client);
        await createPurchaseService(purchaseData);
        await updateClientService(clientData);
        return { body: { message: 'Purchase Created!', purchaseData, clientData } };
    
};

module.exports = { createPurchaseDomain };