const minAge = 18;
const maxAge = 65;

//@ts-check
const calculateAge = (birthDate) => {
    const ageDiff = Date.now() - new Date(birthDate).getTime()
    const ageDiffToDateFormat = new Date(ageDiff);
    return Math.abs((ageDiffToDateFormat.getUTCFullYear() -1970));    
}

const validateParams = (data) => {        
    
    const age = calculateAge(data.birthDate);
    if(age < minAge || age > maxAge) {
        throw new Error("Your age is not allowed for this operation")
    }
    return data;    
}


module.exports = {
    validateParams,
}