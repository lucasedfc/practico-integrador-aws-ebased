//@ts-check
const { ErrorHandled } = require("ebased/util/error");
const { ClientUpdatedEvent } = require("../schema/event/updateClient.event");
const { UpdateClientValidation } = require("../schema/input/updateClient.input");
const { emitClientService } = require("../service/emitClient.service");
const { getClientByIdService } = require("../service/getClientById.service");
const { updateClientService } = require("../service/updateClient.service");


module.exports = async (commandPayload, commandMeta) => {

    try {
        const data = new UpdateClientValidation(commandPayload, commandMeta).get();
        await getClientByIdService(data.dni);
        const clientUpdated = await updateClientService(data);
        await emitClientService(new ClientUpdatedEvent(data, commandMeta));
        return { status: 200, body: clientUpdated };
    } catch (error) {
        throw new ErrorHandled(error.message || 'Unable to update Client', 
        { status: 400, code: 'ERROR', layer: 'CLIENT'});        
    }
}
