//@ts-check
const { validateParams } = require("../helper/validateParams.helper");
const { CreateClientValidation } = require("../schema/input/createClient.input");
const { v4: uuidv4 } = require('uuid');
const { saveClientService } = require('../service/saveClient.service');
const { emitClientService } = require("../service/emitClient.service");
const { ClientCreatedEvent } = require("../schema/event/emitClient.event");
const { ErrorHandled } = require("ebased/util/error");


module.exports = async (commandPayload, commandMeta) => {

    commandPayload.id = uuidv4();        
    const data = new CreateClientValidation(commandPayload, commandMeta).get();  
    
    try {        
        validateParams(data);
        await saveClientService(data);    
        await emitClientService(new ClientCreatedEvent(data, commandMeta));        
        return {status: 200, body: 'Client Created' };   
    } catch (error) {
        throw new ErrorHandled(error.message || 'Unable to create Client' , 
        { status: 400, code: 'ERROR', layer: 'CLIENT'});        
        
    }    

}
