//@ts-check
const config = require('ebased/util/config');
const dynamoDB = require('ebased/service/storage/dynamo');
const CLIENT_TABLE = config.get('CLIENT_TABLE');

const updateClientService = async(data) => {

    const params = {
        TableName: CLIENT_TABLE,
        Key: { dni: data.dni },
        UpdateExpression: 'SET',
        ExpressionAttributeNames: {},
        ExpressionAttributeValues: {},
        ReturnValues: 'ALL_NEW',
      }      

    Object.keys(data).forEach(key => {      
    if(key !== 'dni') {
      params.UpdateExpression += ` #${key}=:${key},`;
      params.ExpressionAttributeNames[`#${key}`] = key;
      params.ExpressionAttributeValues[`:${key}`] = data[key];
    }
  });

  params.UpdateExpression = params.UpdateExpression.slice(0, -1);

  const { Attributes } = await dynamoDB.updateItem(params);
  return Attributes;
}

module.exports = { updateClientService }