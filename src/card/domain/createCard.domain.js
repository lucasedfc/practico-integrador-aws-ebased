// @ts-check
const { ErrorHandled } = require('ebased/util/error');
const { calculateAge } = require('../helper/calculateAge.helper');
const { randomNumber } = require('../helper/randomNumber.helper');
const { CreateCardValidation } = require('../schema/input/createCard.input');

const { createCard } = require('../service/createCard.service');
const MIN_CARD_NUM = 1000;
const MAX_CARD_NUM = 9999;
const MIN_CODE_NUM = 100;
const MAX_CODE_NUM = 999;


module.exports = async (commandPayload, commandMeta) => {

    console.log({ commandPayload });

    const { birthDate, dni } = new CreateCardValidation(commandPayload, commandMeta).get();
    const type = calculateAge(birthDate) > 45 ? 'Gold' : 'Classic';
    const cardNumber = randomNumber(MIN_CARD_NUM, MAX_CARD_NUM) + '-' +
        randomNumber(MIN_CARD_NUM, MAX_CARD_NUM) + '-' +
        randomNumber(MIN_CARD_NUM, MAX_CARD_NUM) + '-' +
        randomNumber(MIN_CARD_NUM, MAX_CARD_NUM);
    const cardCode = randomNumber(MIN_CODE_NUM, MAX_CODE_NUM);
    // @ts-ignore
    const expirationDate = randomNumber(01, 12) + '/' + randomNumber(22, 30);

    const data = { update: { type, cardNumber, cardCode, expirationDate }, dni };

    try {
        await createCard(data);
        return { status: 200, body: 'Card Created' };

    } catch (error) {
        throw new ErrorHandled(error.message || 'Unable to create Card',
            { status: 500, code: 'ERROR', layer: 'CARD' });
    }
}